<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Class AdminController Отображение таблицы с визитами
 * @package App\Http\Controllers
 */
class AdminController extends Controller
{
    /**
     * @var int Число строк на странице
     */
    private $on_page=5;

    /**
     * @param int $page_number страница
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showPage(int $page_number=1){
        if($page_number<1){
            abort(404);
        }
        $data=\App\Facades\JsonRPCServiceFacade::send("getPage", [
            'start' => $this->on_page*($page_number-1),
            'on_page_number'=>$this->on_page,
        ]);

        if(!isset($data['result'])||!$data['result']){
            abort(404);
        }

        $data['result']['pages_count']=ceil( $data['result']['total']/$this->on_page);
        $data['result']['page_number']=$page_number;

        return view('adminlist',$data['result']);
    }

}
