<?php
namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

/**
 * Class JsonRpcClient Клиент - отправка сообщений
 * @package App\Services
 */
class JsonRpcClient
{
    const JSON_RPC_VERSION = '2.0';
    const METHOD_URI = 'jsonserver';

    /**
     * @var Client Клиент
     */
    private $client;

    /**
     * JsonRpcClient constructor.
     * @param string $base_url адрес сервиса
     */
    public function __construct(string $base_url)
    {
        $this->client = new Client([
            'headers' => ['Content-Type' => 'application/json'],
            'base_uri' => $base_url,
        ]);
    }

    /**
     * @param string $method метод запроса
     * @param array $params параметры запроса
     * @return array ответ
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(string $method, array $params): array
    {
        $params['time']=date('Y-m-d H:i:s');
        $response = $this->client->post(self::METHOD_URI, [
            RequestOptions::JSON => [
                'jsonrpc' => self::JSON_RPC_VERSION,
                'id' => time(),
                'method' => $method,
                'params' => $params,
            ]
        ])->getBody()->getContents();

        return json_decode($response, true);
    }
}
