<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;
use App\Services\JsonRpcClient;

/**
 * Class JsonRPCServiceFacade Фасад для сервис-провайдера
 * @package App\Facades
 */
class JsonRPCServiceFacade extends Facade{
    protected static function getFacadeAccessor() { return JsonRpcClient::class; }
}
