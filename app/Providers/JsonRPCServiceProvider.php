<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\JsonRpcClient;

/**
 * Class JsonRPCServiceProvider Сервис-провайдер для json-общения
 * @package App\Providers
 */
class JsonRPCServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(JsonRpcClient::class, function ($app) {
            return new JsonRpcClient(config('services.jsonrpc.base_uri'));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
