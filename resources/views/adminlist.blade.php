<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Таблица доступа</title>
</head>
<body>
<div class="container">
    <h1>Таблица доступа</h1>
    <table class="table table-striped">
        <tr>
            <th>Страница</th>
            <th>Визитов</th>
            <th>Последний визит</th>
        </tr>
        @foreach ($trs as $line_one)
            <tr>
                <td>{{ $line_one['url'] }}</td>
                <td>{{ $line_one['counter'] }}</td>
                <td>{{ $line_one['last_visit'] }}</td>
            </tr>
        @endforeach
    </table>

    @if($pages_count>1)
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                @for ($i=1; $i<=$pages_count; ++$i)
                    <li class="page-item @if($page_number==$i) active @endif"><a class="page-link" href="{{ url("/admin/activity/$i") }}">{{ $i }}</a></li>
                @endfor
            </ul>
        </nav>
    @endif
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
